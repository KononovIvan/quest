package ru.ykt.kvaquest.entity

/**
 * @author Ivan Kononov on 16.02.19
 */
data class AuthResponse(
        val status: String,
        val body: String
)