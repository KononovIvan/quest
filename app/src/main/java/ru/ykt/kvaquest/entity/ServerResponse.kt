package ru.ykt.kvaquest.entity

import com.google.gson.annotations.SerializedName

/**
 * @author Ivan Kononov on 16.02.19
 */
data class ServerResponse<T>(
        val content: T
) {

    fun getData() = content
}