package ru.ykt.kvaquest.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * @author Ivan Kononov on 15.02.19
 */
@Parcelize
data class Quest(
        val id: Long,
        val title: String,
        val text: String,
        val longitude: Double,
        val latitude: Double
) : Parcelable