package ru.ykt.kvaquest.presentation.quests

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * @author Ivan Kononov on 16.02.19
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface QuestsView : MvpView {
    fun showData(show: Boolean, data: List<Any>)
    fun showEmptyProgress(show: Boolean)
    fun showEmptyError(show: Boolean, message: String?)
    fun showEmptyView(show: Boolean)
    fun showRefreshProgress(show: Boolean)
    fun showPageProgress(show: Boolean)
}