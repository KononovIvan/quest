package ru.ykt.kvaquest.presentation.auth

import com.arellomobile.mvp.MvpView
import ru.terrakok.cicerone.Router
import ru.ykt.kvaquest.di.DI
import ru.ykt.kvaquest.presentation.global.BasePresenter
import toothpick.Toothpick
import javax.inject.Inject

/**
 * @author Ivan Kononov on 16.02.19
 */
class AuthFlowPresenter @Inject constructor(
        private val router: Router
) : BasePresenter<MvpView>() {

    override fun onDestroy() {
        Toothpick.closeScope(DI.AUTH_FLOW_SCOPE)
        super.onDestroy()
    }

    fun onExit() = router.exit()
}