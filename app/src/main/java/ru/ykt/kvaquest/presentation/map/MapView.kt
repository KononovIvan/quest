package ru.ykt.kvaquest.presentation.map

import android.location.Location
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * @author Ivan Kononov on 16.02.19
 */
@StateStrategyType(AddToEndSingleStrategy::class)
interface MapView : MvpView{
    fun updateLocation(location: Location)
    fun showData(title: String, text: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun requestPermission(permisions: Array<String>, requestCode: Int)
}