package ru.ykt.kvaquest.presentation.auth

import com.arellomobile.mvp.MvpView
import ru.terrakok.cicerone.Router
import ru.ykt.kvaquest.model.data.AuthHolder
import ru.ykt.kvaquest.model.interactor.RegisterInteractor
import ru.ykt.kvaquest.presentation.global.BasePresenter
import ru.ykt.kvaquest.ui.Screens
import timber.log.Timber
import javax.inject.Inject

/**
 * @author Ivan Kononov on 16.02.19
 */
class RegisterPresenter @Inject constructor(
        private val router: Router,
        private val interactor: RegisterInteractor,
        private val authHolder: AuthHolder
) : BasePresenter<MvpView>() {

    fun onRegisterClicked(username: String, password: String) {
        Timber.i("on register")
        interactor.register(username, password)
                .subscribe(
                        {
                            if (it.body == "ok"){
                                authHolder.token = "fh8392fioew02"
                                router.newRootScreen(Screens.MainFlowScreen)
                            }
                            else
                                Timber.e(it.body)
                        },
                        {
                            Timber.e(it)
                        }
                )
                .connect()
    }


    fun onBackPressed() {
        Timber.i("onBack")
        router.exit()
    }
}