package ru.ykt.kvaquest.presentation.main

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpView
import ru.terrakok.cicerone.Router
import ru.ykt.kvaquest.model.data.AuthHolder
import ru.ykt.kvaquest.model.system.FlowRouter
import ru.ykt.kvaquest.presentation.global.BasePresenter
import ru.ykt.kvaquest.ui.Screens
import javax.inject.Inject

/**
 * @author Ivan Kononov on 16.02.19
 */
@InjectViewState
class MainPresenter @Inject constructor(
        private val authHolder: AuthHolder,
        private val router: Router
) : BasePresenter<MainView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        if(authHolder.token.isNullOrEmpty())
            router.newRootScreen(Screens.AuthFlowScreen)
        else
            viewState.initMainScreen()
    }

}