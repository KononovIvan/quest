package ru.ykt.kvaquest.presentation.main

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * @author Ivan Kononov on 16.02.19
 */
@StateStrategyType(OneExecutionStateStrategy::class)
interface MainView : MvpView {
    fun initMainScreen()
}