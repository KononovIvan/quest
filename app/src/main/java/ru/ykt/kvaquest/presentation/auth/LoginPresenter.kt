package ru.ykt.kvaquest.presentation.auth

import com.arellomobile.mvp.MvpView
import ru.terrakok.cicerone.Router
import ru.ykt.kvaquest.presentation.global.BasePresenter
import timber.log.Timber
import javax.inject.Inject

/**
 * @author Ivan Kononov on 16.02.19
 */
class LoginPresenter @Inject constructor(
        private val router: Router
) : BasePresenter<MvpView>() {

    fun onLoginClicked(username: String, password: String) {

    }

    fun onBackPressed() {
        Timber.i("onBack")
        router.exit()
    }
}