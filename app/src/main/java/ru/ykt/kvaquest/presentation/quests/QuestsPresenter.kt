package ru.ykt.kvaquest.presentation.quests

import com.arellomobile.mvp.InjectViewState
import ru.terrakok.cicerone.Router
import ru.ykt.kvaquest.entity.Quest
import ru.ykt.kvaquest.model.interactor.QuestsInteractor
import ru.ykt.kvaquest.presentation.global.BasePresenter
import ru.ykt.kvaquest.presentation.global.Paginator
import ru.ykt.kvaquest.ui.Screens
import timber.log.Timber
import javax.inject.Inject

/**
 * @author Ivan Kononov on 16.02.19
 */
@InjectViewState
class QuestsPresenter @Inject constructor(
        private val interactor: QuestsInteractor,
        private val router: Router
) : BasePresenter<QuestsView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        refresh()
    }

    private val paginator = Paginator(
            {
                interactor.getQuests(it)
            },
            object : Paginator.ViewController<Quest> {
                override fun showEmptyProgress(show: Boolean) {
                    viewState.showEmptyProgress(show)
                }

                override fun showEmptyError(show: Boolean, error: Throwable?) {
                    viewState.showEmptyError(show, null)
                    error?.let { Timber.e(it) }
                }

                override fun showEmptyView(show: Boolean) {
                    viewState.showEmptyView(show)
                }

                override fun showData(show: Boolean, data: List<Quest>) {
                    viewState.showData(show, data)
                }

                override fun showErrorMessage(error: Throwable) {
                    Timber.e(error)
                }

                override fun showRefreshProgress(show: Boolean) {
                    viewState.showRefreshProgress(show)
                }

                override fun showPageProgress(show: Boolean) {
                    viewState.showPageProgress(show)
                }
            }
    )

    fun refresh() = paginator.refresh()
    fun loadNewPage() = paginator.loadNewPage()
    fun onQuestClicked(quest: Quest) { router.navigateTo(Screens.MapScreen(quest)) }
    fun onBackPressed() = router.exit()
}