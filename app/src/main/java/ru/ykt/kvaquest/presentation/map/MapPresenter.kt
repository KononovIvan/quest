package ru.ykt.kvaquest.presentation.map

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.arellomobile.mvp.InjectViewState
import ru.terrakok.cicerone.Router
import ru.ykt.kvaquest.presentation.global.BasePresenter
import timber.log.Timber
import javax.inject.Inject

/**
 * @author Ivan Kononov on 16.02.19
 */
@InjectViewState
class MapPresenter @Inject constructor(
        private val context: Context,
        private val router: Router,
        private val locationManager: LocationManager
): BasePresenter<MapView>(){

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED)
            viewState.requestPermission(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1)

        locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)?.let {
            viewState.updateLocation(it)
        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000L, 0.5f,
                object : LocationListener {
                    override fun onLocationChanged(location: Location) {
                        Timber.i("onLocationChanged ${location.longitude} ${location.latitude}")
                        viewState.updateLocation(location)
                    }

                    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}

                    override fun onProviderEnabled(provider: String?) {}

                    override fun onProviderDisabled(provider: String?) {}
                }
        )
    }

    fun onBackPressed() = router.exit()
}