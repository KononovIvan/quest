package ru.ykt.kvaquest.di

/**
 * @author Ivan Kononov on 16.02.19
 */
object DI {
    const val APP_SCOPE = "app scope"
    const val SERVER_SCOPE = "server scope"
    const val AUTH_FLOW_SCOPE = "auth scope"
}