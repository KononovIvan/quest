package ru.ykt.kvaquest.di.modules

import android.content.Context
import android.location.LocationManager
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.ykt.kvaquest.model.data.AuthHolder
import ru.ykt.kvaquest.model.data.Prefs
import ru.ykt.kvaquest.model.system.AppSchedulers
import ru.ykt.kvaquest.model.system.SchedulersProvider
import toothpick.config.Module

/**
 * @author Ivan Kononov on 16.02.19
 */
class AppModule(context: Context) : Module() {
    init {
        bind(Context::class.java).toInstance(context)
        bind(SchedulersProvider::class.java).toInstance(AppSchedulers())

        //Navigation
        val cicerone = Cicerone.create()
        bind(Router::class.java).toInstance(cicerone.router)
        bind(NavigatorHolder::class.java).toInstance(cicerone.navigatorHolder)

        //Auth
        bind(AuthHolder::class.java).to(Prefs::class.java).singletonInScope()

        //Location
        bind(LocationManager::class.java).toInstance(context.getSystemService(Context.LOCATION_SERVICE) as LocationManager)
    }
}