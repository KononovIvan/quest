package ru.ykt.kvaquest.di.modules

import com.google.gson.Gson
import okhttp3.OkHttpClient
import ru.ykt.kvaquest.di.ServerPath
import ru.ykt.kvaquest.di.provider.ApiProvider
import ru.ykt.kvaquest.di.provider.GsonProvider
import ru.ykt.kvaquest.di.provider.OkHttpClientProvider
import ru.ykt.kvaquest.model.interactor.QuestsInteractor
import ru.ykt.kvaquest.model.repository.QuestsRepository
import ru.ykt.kvaquest.model.data.server.QuestApi
import ru.ykt.kvaquest.model.interactor.RegisterInteractor
import ru.ykt.kvaquest.model.repository.RegisterRepository
import toothpick.config.Module

/**
 * @author Ivan Kononov on 16.02.19
 */
class ServerModule(serverPath: String) : Module() {
    init {
        bind(String::class.java).withName(ServerPath::class.java).toInstance(serverPath)
        bind(Gson::class.java).toProvider(GsonProvider::class.java).providesSingletonInScope()
        bind(OkHttpClient::class.java).toProvider(OkHttpClientProvider::class.java).providesSingletonInScope()
        bind(QuestApi::class.java).toProvider(ApiProvider::class.java).providesSingletonInScope()

        bind(QuestsInteractor::class.java)
        bind(QuestsRepository::class.java)

        bind(RegisterInteractor::class.java)
        bind(RegisterRepository::class.java)
    }
}