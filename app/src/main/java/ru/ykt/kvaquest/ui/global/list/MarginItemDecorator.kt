package ru.ykt.kvaquest.ui.global.list

import android.graphics.Rect
import androidx.recyclerview.widget.RecyclerView
import android.view.View

/**
 * @author Ivan Kononov on 15.01.19
 */
class MarginItemDecorator(
        private val marginTopPx: Int,
        private val marginBottomPx: Int,
        private val marginLeftPx: Int,
        private val marginRightPx: Int
) : RecyclerView.ItemDecoration() {

    constructor(marginPx: Int) : this(marginPx, marginPx, marginPx, marginPx)

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)

        if (parent.getChildLayoutPosition(view) == 0) outRect.top = marginTopPx
        outRect.left = marginLeftPx
        outRect.right = marginRightPx
        outRect.bottom = marginBottomPx
    }
}