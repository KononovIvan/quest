package ru.ykt.kvaquest.ui.auth

import android.os.Bundle
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.commands.BackTo
import ru.terrakok.cicerone.commands.Replace
import ru.ykt.kvaquest.di.DI
import ru.ykt.kvaquest.di.modules.FlowNavigationModule
import ru.ykt.kvaquest.extension.hideKeyboard
import ru.ykt.kvaquest.presentation.auth.AuthFlowPresenter
import ru.ykt.kvaquest.ui.Screens
import ru.ykt.kvaquest.ui.global.FlowFragment
import toothpick.Toothpick

/**
 * @author Ivan Kononov on 16.02.19
 */
class AuthFlowFragment : FlowFragment(), MvpView {

    @InjectPresenter
    lateinit var presenter: AuthFlowPresenter

    @ProvidePresenter
    fun providePresenter() = Toothpick
            .openScope(DI.AUTH_FLOW_SCOPE)
            .getInstance(AuthFlowPresenter::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        prepareScope(isFirstLaunch(savedInstanceState))
        super.onCreate(savedInstanceState)
        if (childFragmentManager.fragments.isEmpty()) {
            navigator.applyCommands(arrayOf(BackTo(null), Replace(Screens.StartScreen)))
        }
    }

    private fun prepareScope(firstLaunch: Boolean) {
        val scope = Toothpick.openScopes(DI.SERVER_SCOPE, DI.AUTH_FLOW_SCOPE)
        if (firstLaunch) {
            scope.installModules(
                    FlowNavigationModule(scope.getInstance(Router::class.java))
            )
        }
        Toothpick.inject(this, scope)
    }

    override fun onExit() {
        activity?.hideKeyboard()
        presenter.onExit()
    }
}