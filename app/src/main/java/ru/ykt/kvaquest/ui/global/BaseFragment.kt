package ru.ykt.kvaquest.ui.global

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ru.ykt.kvaquest.QuestApp
import ru.ykt.kvaquest.ui.global.mvp.MvpAppCompatFragment

/**
 * @author Ivan Kononov on 09.11.18
 */
abstract class BaseFragment : MvpAppCompatFragment() {
    companion object {
        private const val STATE_LAUNCH_FLAG = "state_launch_flag"
        private const val PROGRESS_TAG = "bf_progress"
    }

    abstract val layoutRes: Int

    private var instanceStateSaved: Boolean = false

    private val viewHandler = Handler()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(layoutRes, container, false)

    override fun onResume() {
        super.onResume()
        instanceStateSaved = false
    }

    //fix for async views (like swipeToRefresh and RecyclerView)
    //if synchronously call actions on swipeToRefresh in sequence show and hide then swipeToRefresh will not hidden
    protected fun postViewAction(action: () -> Unit) {
        viewHandler.post(action)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewHandler.removeCallbacksAndMessages(null)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(STATE_LAUNCH_FLAG, QuestApp.appCode)
        instanceStateSaved = true
    }

    protected fun showProgressDialog(progress: Boolean) {
        if (!isAdded || instanceStateSaved) return

        val fragment = childFragmentManager.findFragmentByTag(PROGRESS_TAG)
        if (fragment != null && !progress) {
            (fragment as ProgressDialog).dismissAllowingStateLoss()
            childFragmentManager.executePendingTransactions()
        } else if (fragment == null && progress) {
            ProgressDialog().show(childFragmentManager, PROGRESS_TAG)
            childFragmentManager.executePendingTransactions()
        }
    }

    protected fun isFirstLaunch(savedInstanceState: Bundle?): Boolean {
        val savedAppCode = savedInstanceState?.getString(STATE_LAUNCH_FLAG)
        return savedAppCode != QuestApp.appCode
    }

    open fun onBackPressed() {}
}