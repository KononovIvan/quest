package ru.ykt.kvaquest.ui

import android.content.Context
import android.content.Intent
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.ykt.kvaquest.MainActivity
import ru.ykt.kvaquest.entity.Quest
import ru.ykt.kvaquest.ui.auth.AuthFlowFragment
import ru.ykt.kvaquest.ui.auth.RegisterFragment
import ru.ykt.kvaquest.ui.auth.LoginFragment
import ru.ykt.kvaquest.ui.auth.StartFragment
import ru.ykt.kvaquest.ui.map.MapFragment
import ru.ykt.kvaquest.ui.quests.QuestsFragment

/**
 * @author Ivan Kononov on 16.02.19
 */
object Screens {

    object MainFlowScreen : SupportAppScreen() {
        override fun getActivityIntent(context: Context?) = Intent(context, MainActivity::class.java)
    }

    object QuestsScreen : SupportAppScreen() {
        override fun getFragment() = QuestsFragment()
    }

    object AuthFlowScreen : SupportAppScreen() {
        override fun getFragment() = AuthFlowFragment()
    }

    object StartScreen : SupportAppScreen() {
        override fun getFragment() = StartFragment()
    }

    object LoginScreen : SupportAppScreen() {
        override fun getFragment() = LoginFragment()
    }

    object RegisterScreen : SupportAppScreen() {
        override fun getFragment() = RegisterFragment()
    }

    data class MapScreen(val quest: Quest) : SupportAppScreen() {
        override fun getFragment() = MapFragment.newInstance(quest)
    }
}