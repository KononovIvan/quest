package ru.ykt.kvaquest.ui.global.list

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup

import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import ru.ykt.kvaquest.R

import ru.ykt.kvaquest.extension.inflate

class ProgressAdapterDelegate : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) =
            items[position] is ProgressItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
            ViewHolder(parent.inflate(R.layout.item_progress))

    override fun onBindViewHolder(items: MutableList<Any>,
                                  position: Int,
                                  viewHolder: RecyclerView.ViewHolder,
                                  payloads: List<Any>) {}

    private inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}