package ru.ykt.kvaquest.ui.quests

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import kotlinx.android.synthetic.main.layout_base_list.*
import kotlinx.android.synthetic.main.layout_zero.*
import ru.ykt.kvaquest.R
import ru.ykt.kvaquest.di.DI
import ru.ykt.kvaquest.extension.visible
import ru.ykt.kvaquest.presentation.quests.QuestsPresenter
import ru.ykt.kvaquest.presentation.quests.QuestsView
import ru.ykt.kvaquest.ui.global.BaseFragment
import ru.ykt.kvaquest.ui.global.ZeroViewHolder
import ru.ykt.kvaquest.ui.global.list.MarginItemDecorator
import ru.ykt.kvaquest.ui.global.list.ProgressAdapterDelegate
import ru.ykt.kvaquest.ui.global.list.ProgressItem
import ru.ykt.kvaquest.ui.global.list.QuestItemDelegateAdapter
import toothpick.Toothpick

/**
 * @author Ivan Kononov on 16.02.19
 */
class QuestsFragment : BaseFragment(), QuestsView {
    override val layoutRes = R.layout.fragment_quests

    @InjectPresenter
    lateinit var presenter: QuestsPresenter

    @ProvidePresenter
    fun providePresenter() =
            Toothpick.openScope(DI.SERVER_SCOPE)
                    .getInstance(QuestsPresenter::class.java)

    private val adapter: MainAdapter by lazy { MainAdapter() }
    private var zeroViewHolder: ZeroViewHolder? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        swipeToRefresh.setOnRefreshListener { presenter.refresh() }

        with(recyclerView) {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            addItemDecoration(MarginItemDecorator(context.resources.getDimensionPixelSize(R.dimen.space_normal)))
            adapter = this@QuestsFragment.adapter
        }

        zeroViewHolder = ZeroViewHolder(zeroLayout)
    }

    override fun showData(show: Boolean, data: List<Any>) {
        recyclerView.visible(show)
        postViewAction { adapter.setData(data) }
    }

    override fun showEmptyProgress(show: Boolean) {
        progressView.visible(show)

        swipeToRefresh.isEnabled = !show
        postViewAction { swipeToRefresh.isRefreshing = false }
    }

    override fun showRefreshProgress(show: Boolean) {
        postViewAction { swipeToRefresh.isRefreshing = show }
    }

    override fun showPageProgress(show: Boolean) {
        postViewAction { adapter.showProgress(show) }
    }

    override fun showEmptyView(show: Boolean) {
        if (show) {
            zeroViewHolder?.showEmptyData(
                    getString(R.string.empty_data),
                    imageResId = R.drawable.ic_zero_search_150dp
            )
        } else zeroViewHolder?.hide()
    }

    override fun showEmptyError(show: Boolean, message: String?) {
        if (show) {
            zeroViewHolder?.showEmptyError { presenter.refresh() }
        } else {
            zeroViewHolder?.hide()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        presenter.onBackPressed()
    }

    private inner class MainAdapter : ListDelegationAdapter<MutableList<Any>>() {

        init {
            items = mutableListOf()
            delegatesManager
                    .addDelegate(QuestItemDelegateAdapter { presenter.onQuestClicked(it) })
                    .addDelegate(ProgressAdapterDelegate())
        }

        fun setData(data: List<Any>) {
            items.clear()
            items.addAll(data)

            notifyDataSetChanged()
        }

        fun showProgress(isVisible: Boolean) {
            val currentProgress = isProgress()

            if (isVisible && !currentProgress) items.add(ProgressItem())
            else if (!isVisible && currentProgress) items.remove(items.last())
        }

        private fun isProgress() = items.isNotEmpty() && items.last() is ProgressItem

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any?>) {
            super.onBindViewHolder(holder, position)
            if(position == items.size - 5) presenter.loadNewPage()
        }
    }
}