package ru.ykt.kvaquest.ui.auth

import android.os.Bundle
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_register.*
import ru.ykt.kvaquest.R
import ru.ykt.kvaquest.di.DI
import ru.ykt.kvaquest.presentation.auth.RegisterPresenter
import ru.ykt.kvaquest.ui.global.BaseFragment
import toothpick.Toothpick

/**
 * @author Ivan Kononov on 16.02.19
 */
class RegisterFragment : BaseFragment(), MvpView{

    override val layoutRes = R.layout.fragment_register

    @InjectPresenter
    lateinit var presenter: RegisterPresenter

    @ProvidePresenter
    fun providePresenter() =
            Toothpick.openScope(DI.AUTH_FLOW_SCOPE)
                    .getInstance(RegisterPresenter::class.java)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        register_button.setOnClickListener {
            presenter.onRegisterClicked(
                    username_edit_text.text.toString(),
                    password_edit_text.text.toString()
            ) }
    }

    override fun onBackPressed() {
        super.onBackPressed()

        presenter.onBackPressed()

    }
}