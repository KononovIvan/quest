package ru.ykt.kvaquest.ui.auth

import android.os.Bundle
import com.arellomobile.mvp.MvpView
import kotlinx.android.synthetic.main.fragment_start.*
import ru.ykt.kvaquest.R
import ru.ykt.kvaquest.di.DI
import ru.ykt.kvaquest.model.system.FlowRouter
import ru.ykt.kvaquest.ui.Screens
import ru.ykt.kvaquest.ui.global.BaseFragment
import toothpick.Toothpick
import javax.inject.Inject

/**
 * @author Ivan Kononov on 16.02.19
 */
class StartFragment : BaseFragment(), MvpView {
    override val layoutRes = R.layout.fragment_start

    @Inject
    lateinit var router: FlowRouter

    override fun onCreate(savedInstanceState: Bundle?) {
        Toothpick.inject(this, Toothpick.openScope(DI.AUTH_FLOW_SCOPE))
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        login.setOnClickListener { router.startFlow(Screens.LoginScreen) }
        signup.setOnClickListener { router.startFlow(Screens.RegisterScreen) }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        router.finishFlow()
    }
}