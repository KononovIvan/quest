package ru.ykt.kvaquest.ui.global.list

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import kotlinx.android.synthetic.main.item_quest.view.*
import ru.ykt.kvaquest.R
import ru.ykt.kvaquest.entity.Quest
import ru.ykt.kvaquest.extension.inflate

/**
 * @author Ivan Kononov on 16.02.19
 */
class QuestItemDelegateAdapter(
        private val clickListener: (Quest) -> Unit
) : AdapterDelegate<MutableList<Any>>() {

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
            ViewHolder(parent.inflate(R.layout.item_quest))

    override fun isForViewType(items: MutableList<Any>, position: Int): Boolean =
            items[position] is Quest

    override fun onBindViewHolder(
            items: MutableList<Any>,
            position: Int,
            holder: RecyclerView.ViewHolder,
            payloads: MutableList<Any>
    ) = (holder as ViewHolder).bind(items[position] as Quest)

    private inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private lateinit var item: Quest

        init {
            view.setOnClickListener { clickListener.invoke(item) }
        }

        fun bind(data: Quest) {
            item = data
            itemView.title_text_view.text = data.title
            itemView.body_text_view.text = data.text
        }
    }
}