package ru.ykt.kvaquest.ui.map

import android.location.Location
import android.os.Bundle
import androidx.core.app.ActivityCompat
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_map.*
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay
import ru.ykt.kvaquest.R
import ru.ykt.kvaquest.di.DI
import ru.ykt.kvaquest.entity.Quest
import ru.ykt.kvaquest.presentation.map.MapPresenter
import ru.ykt.kvaquest.presentation.map.MapView
import ru.ykt.kvaquest.ui.global.BaseFragment
import toothpick.Toothpick

/**
 * @author Ivan Kononov on 16.02.19
 */
class MapFragment : BaseFragment(), MapView {

    companion object {
        private const val ARG_QUEST = "quest_tag"
        fun newInstance(quest: Quest) = MapFragment().apply {
            arguments = Bundle().apply {
                putParcelable(ARG_QUEST, quest)
            }
        }
    }

    private val quest by lazy { arguments!!.getParcelable<Quest>(ARG_QUEST)!! }

    override val layoutRes = R.layout.fragment_map

    @InjectPresenter
    lateinit var presenter: MapPresenter

    @ProvidePresenter
    fun providePresenter() =
            Toothpick.openScope(DI.SERVER_SCOPE)
                    .getInstance(MapPresenter::class.java)

    private lateinit var locationProvider: GpsMyLocationProvider

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        map.setTileSource(TileSourceFactory.MAPNIK)
        map.setBuiltInZoomControls(true)
        map.setMultiTouchControls(true)
        map.controller.setZoom(15.0)

        locationProvider = GpsMyLocationProvider(context)
        val locationOverlay = MyLocationNewOverlay(locationProvider, map)
        locationOverlay.enableMyLocation()
        map.overlays.add(locationOverlay)

        showData(quest.title, quest.text)
    }

    override fun requestPermission(permisions: Array<String>, requestCode: Int) {
        ActivityCompat.requestPermissions(activity!!, permisions, requestCode)
    }

    override fun updateLocation(location: Location) {
        locationProvider.onLocationChanged(location)
        map.controller.setCenter(GeoPoint(location.latitude, location.longitude))
    }

    override fun showData(title: String, text: String) {
        title_text_view.text = title
        body_text_view.text = text
    }

    override fun onBackPressed() {
        super.onBackPressed()
        presenter.onBackPressed()
    }

    override fun onResume() {
        super.onResume()
        map.onResume()
    }

    override fun onPause() {
        super.onPause()
        map.onPause()
    }
}