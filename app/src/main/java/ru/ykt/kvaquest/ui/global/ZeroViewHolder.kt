package ru.ykt.kvaquest.ui.global

import android.view.ViewGroup
import kotlinx.android.synthetic.main.layout_zero.view.*
import ru.ykt.kvaquest.R
import ru.ykt.kvaquest.extension.visible

/**
 * @author Ivan Kononov on 17.11.18
 */
class ZeroViewHolder(private val view: ViewGroup) {
    private val res = view.resources

    fun showEmptyError(clickListener: () -> Unit) {
        with(view) {
            zeroTitleTextView.text = res.getString(R.string.empty_error)
            zeroDescriptionTextView.text = res.getString(R.string.empty_error_description)
//            zeroImageView.setImageResource(R.drawable.ic_zero_socket_150dp)

//            zeroButton.text = res.getString(R.string.empty_error_button)
//            zeroButton.setOnClickListener { clickListener.invoke() }
//            zeroButton.visible(true)

            visible(true)
        }
    }

    fun showEmptyData(title: String, msg: String = "", imageResId: Int, buttonText: String = "", clickListener: () -> Unit = {}) {
        with(view) {
            zeroTitleTextView.text = title
            zeroDescriptionTextView.text = msg
            zeroImageView.setImageResource(imageResId)

            zeroButton.text = buttonText
            zeroButton.setOnClickListener { clickListener.invoke() }
            zeroButton.visible(buttonText.isNotEmpty())

            visible(true)
        }
    }

    fun hide() {
        view.visible(false)
    }
}