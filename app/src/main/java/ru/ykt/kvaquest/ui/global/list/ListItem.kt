package ru.ykt.kvaquest.ui.global.list

/**
 * @author Ivan Kononov on 16.02.19
 */
sealed class ListItem {
    class QuestItem(val id: Int, val title: String, val text: String) : ListItem()
}