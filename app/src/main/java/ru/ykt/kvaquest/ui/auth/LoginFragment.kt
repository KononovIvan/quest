package ru.ykt.kvaquest.ui.auth

import android.os.Bundle
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_login.*
import ru.ykt.kvaquest.R
import ru.ykt.kvaquest.di.DI
import ru.ykt.kvaquest.presentation.auth.LoginPresenter
import ru.ykt.kvaquest.ui.global.BaseFragment
import toothpick.Toothpick

/**
 * @author Ivan Kononov on 16.02.19
 */
class LoginFragment : BaseFragment(), MvpView{

    override val layoutRes = R.layout.fragment_login

    @InjectPresenter
    lateinit var presenter: LoginPresenter

    @ProvidePresenter
    fun providePresenter() =
            Toothpick.openScope(DI.AUTH_FLOW_SCOPE)
                    .getInstance(LoginPresenter::class.java)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        login_button.setOnClickListener {
            presenter.onLoginClicked(
                    username_edit_text.text.toString(),
                    password_edit_text.text.toString()
            )
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        presenter.onBackPressed()
    }
}