package ru.ykt.kvaquest.model.system

import io.reactivex.Scheduler

/**
 * @author Ivan Kononov on 09.11.18
 */
interface SchedulersProvider {
    fun ui(): Scheduler
    fun computation(): Scheduler
    fun trampoline(): Scheduler
    fun newThread(): Scheduler
    fun io(): Scheduler
}