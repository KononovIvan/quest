package ru.ykt.kvaquest.model.data.server

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import ru.ykt.kvaquest.entity.AuthResponse
import ru.ykt.kvaquest.entity.Quest
import ru.ykt.kvaquest.entity.ServerResponse

/**
 * @author Ivan Kononov on 15.02.19
 */
interface QuestApi {
    //http://ogoyukin.ykt.ru:8081/

    companion object {
        private const val API_VERSION = "api"
        const val baseUrl = "http://ogoyukin.ykt.ru:8081/"
    }

    @GET("$API_VERSION/task/list")
    fun getQuests(
            @Query("page") page: Int
//            @Header("JSESSION") token: String
    ): Single<ServerResponse<List<Quest>>>

    @POST("$API_VERSION/register")
    fun register(
            @Query("username") username: String,
            @Query("password") password: String
    ): Single<AuthResponse>

    @POST("$API_VERSION/login")
    fun login(
            @Query("username") username: String,
            @Query("password") password: String
    ): Single<AuthResponse>

    @POST("$API_VERSION/logout")
    fun logout(): Single<AuthResponse>
}