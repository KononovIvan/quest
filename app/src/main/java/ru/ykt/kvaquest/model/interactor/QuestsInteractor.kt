package ru.ykt.kvaquest.model.interactor

import io.reactivex.Single
import ru.ykt.kvaquest.entity.Quest
import ru.ykt.kvaquest.model.repository.QuestsRepository
import javax.inject.Inject

/**
 * @author Ivan Kononov on 16.02.19
 */
class QuestsInteractor @Inject constructor(
        private val repository: QuestsRepository
){
    fun getQuests(page: Int): Single<List<Quest>> = repository.getQuests(page)
}