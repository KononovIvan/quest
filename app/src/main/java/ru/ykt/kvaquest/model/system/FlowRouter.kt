package ru.ykt.kvaquest.model.system

import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppScreen

class FlowRouter(private val appRouter: Router) : Router() {

    fun startFlow(screen: SupportAppScreen) {
        appRouter.navigateTo(screen)
    }

    fun newRootFlow(screen: SupportAppScreen) {
        appRouter.newRootScreen(screen)
    }

    fun replaceFlow(screen: SupportAppScreen) {
        appRouter.replaceScreen(screen)
    }

    fun finishFlow() {
        appRouter.exit()
    }
}