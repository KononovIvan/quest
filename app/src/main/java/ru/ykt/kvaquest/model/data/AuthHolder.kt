package ru.ykt.kvaquest.model.data

/**
 * @author Ivan Kononov
 */
interface AuthHolder {
    var serverPath: String
    var token: String?
    var name: String?
}