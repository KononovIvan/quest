package ru.ykt.kvaquest.model.repository

import io.reactivex.Single
import ru.ykt.kvaquest.entity.Quest
import ru.ykt.kvaquest.model.data.server.QuestApi
import ru.ykt.kvaquest.model.system.SchedulersProvider
import javax.inject.Inject

/**
 * @author Ivan Kononov on 16.02.19
 */
class QuestsRepository @Inject constructor(
        private val api: QuestApi,
        private val schedulers: SchedulersProvider
) {
    fun getQuests(page: Int, pageSize: Int = 10): Single<List<Quest>> =
            api.getQuests(page)
                    .map { it.getData() }
                    .subscribeOn(schedulers.io())
                    .observeOn(schedulers.ui())
}