package ru.ykt.kvaquest.model.interactor

import ru.ykt.kvaquest.model.repository.RegisterRepository
import javax.inject.Inject

/**
 * @author Ivan Kononov on 16.02.19
 */
class RegisterInteractor @Inject constructor(
        private val repository: RegisterRepository
) {

    fun register(username: String, password: String) =
            repository.register(username, password)
}