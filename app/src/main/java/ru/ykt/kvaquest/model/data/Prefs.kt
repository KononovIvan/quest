package ru.ykt.kvaquest.model.data

import android.content.Context
import ru.ykt.kvaquest.model.data.server.QuestApi
import javax.inject.Inject

/**
 * @author Ivan Kononov
 */
class Prefs @Inject constructor(
        private val context: Context
) : AuthHolder {

    private val AUTH_DATA = "auth_data"
    private val KEY_SERVER_PATH = "ad_server_path"
    private val KEY_TOKEN = "ad_token"
    private val KEY_NAME = "ad_name"

    private fun getSharedPreferences(prefsName: String) = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE)

    override var serverPath: String
        get() = getSharedPreferences(AUTH_DATA).getString(KEY_SERVER_PATH, null) ?: QuestApi.baseUrl
        set(value) {
            getSharedPreferences(AUTH_DATA).edit().putString(KEY_SERVER_PATH, value).apply()
        }

    override var token: String?
        get() = getSharedPreferences(AUTH_DATA).getString(KEY_TOKEN, null)
        set(value) {
            getSharedPreferences(AUTH_DATA).edit().putString(KEY_TOKEN, value).apply()
        }

    override var name: String?
        get() = getSharedPreferences(AUTH_DATA).getString(KEY_NAME, null)
        set(value) {
            getSharedPreferences(AUTH_DATA).edit().putString(KEY_NAME, value).apply()
        }

    fun clear() = getSharedPreferences(AUTH_DATA).edit().clear().apply()
}