package ru.ykt.kvaquest.model.repository

import ru.ykt.kvaquest.model.data.server.QuestApi
import ru.ykt.kvaquest.model.system.SchedulersProvider
import javax.inject.Inject

/**
 * @author Ivan Kononov on 16.02.19
 */
class RegisterRepository @Inject constructor(
        private val api: QuestApi,
        private val schedulers: SchedulersProvider
) {

    fun register(username: String, password: String) =
            api.register(username, password)
                    .subscribeOn(schedulers.io())
                    .observeOn(schedulers.ui())
}