package ru.ykt.kvaquest

import android.app.Application
import ru.ykt.kvaquest.di.DI
import ru.ykt.kvaquest.di.modules.AppModule
import ru.ykt.kvaquest.di.modules.ServerModule
import ru.ykt.kvaquest.model.data.server.QuestApi
import timber.log.Timber
import toothpick.Toothpick
import toothpick.configuration.Configuration
import java.util.*

/**
 * @author Ivan Kononov on 16.02.19
 */
class QuestApp : Application() {

    override fun onCreate() {
        super.onCreate()
        appCode = UUID.randomUUID().toString()

        initToothpick()
        initScopes()
        initLogger()
    }

    private fun initToothpick() {
        if (BuildConfig.DEBUG) {
            Toothpick.setConfiguration(Configuration.forDevelopment().preventMultipleRootScopes())
        } else {
            Toothpick.setConfiguration(Configuration.forProduction().disableReflection())
        }
    }

    private fun initScopes() {
        val appScope = Toothpick.openScope(DI.APP_SCOPE)
        appScope.installModules(AppModule(this))

        val serverScope = Toothpick.openScopes(DI.APP_SCOPE, DI.SERVER_SCOPE)
        serverScope.installModules(ServerModule(QuestApi.baseUrl))
    }

    private fun initLogger() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    companion object {
        lateinit var appCode: String
            private set
    }

}